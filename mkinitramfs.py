#!/usr/bin/python3

import os
import os.path
import re
import shutil
import sys
from subprocess import Popen, PIPE

def readLines(
    _FILE,
):
    return [ l.rstrip() for l in _FILE.readlines() ]

def readLines2(
    _FILE,
):
    return [ l.decode( 'utf-8' ) for l in readLines( _FILE ) ]

def findModulePathSet(
    _MODULE_LIST,
):
    modulePathSet = set()

    for MODULE in _MODULE_LIST:
        find = Popen(
            [
                'find',
                LINUX_DIR + '/',
                '-name',
                MODULE + '.ko',
            ],
            stdout = PIPE,
        )

        pathList = readLines2( find.stdout )

        length = len( pathList )
        if length > 1:
            quit(
                '%(module)s が複数存在:%(pathList)s'
                % {
                    'module' : MODULE,
                    'pathList' : pathList,
                }
            )
        elif length < 1:
            quit(
                '%(module)s が存在しない'
                % {
                    'module' : MODULE,
                }
            )

        [ PATH ] = pathList

        modulePathSet.add( PATH )

        modulePathSet |= findModulePathSet(
            getDependModuleList(
                PATH,
            )
        )

    return modulePathSet

def getDependModuleList(
    _MODULE_PATH,
):
    modinfo = Popen(
        [
            'modinfo',
            _MODULE_PATH,
        ],
        stdout = PIPE,
    )

    pattern = re.compile( '^depends:\s*(.+)' )
    for l in readLines2( modinfo.stdout ):
        result = pattern.match( l )
        if result is not None:
            return result.group( 1 ).split( ',' )

    return []

SCRIPT = sys.argv.pop( 0 )

try:
    [
        LINUX_DIR,
        TEMPLATE_DIR,
        MODULE_LIST,
        TMP_DIR,
        OUTPUT_FILE
    ] = sys.argv
except ValueError:
    quit(
        '使い方 : %(script)s LINUX_DIR TEMPLATE_DIR MODULE_LIST TMP_DIR OUTPUT_FILE'
        % {
            'script' : SCRIPT
        }
    )

if os.path.exists( TMP_DIR ):
    quit(
        '一時ディレクトリのパス %(tmpDir)s が存在している'
        % {
            'tmpDir' : TMP_DIR,
        }
    )

if os.path.exists( OUTPUT_FILE ):
    quit(
        '出力ファイルのパス %(outputFile)s が存在している'
        % {
            'outputFile' : OUTPUT_FILE,
        }
    )

f = open(
    MODULE_LIST,
)
moduleList = readLines( f )
f.close()

modulePathSet = findModulePathSet(
    moduleList,
)

Popen(
    [
        'cp',
        '-r',
        TEMPLATE_DIR,
        TMP_DIR,
    ],
).wait()

for modulePath in modulePathSet:
    shutil.copy(
        modulePath,
        TMP_DIR + '/lib/modules/',
    )

find = Popen(
    [
        'find',
        '-print0',
    ],
    cwd = TMP_DIR,
    stdout = PIPE,
)

cpio = Popen(
    [
        'cpio',
        '--null',
        '-ov',
        '--format=newc',
    ],
    cwd = TMP_DIR,
    stdin = find.stdout,
    stdout = PIPE,
)

output = open(
    OUTPUT_FILE,
    'w',
)
gzip = Popen(
    [
        'gzip',
        '-9',
    ],
    stdin = cpio.stdout,
    stdout = output,
)
output.close()
find.wait()
cpio.wait()
gzip.wait()

Popen(
    [
        'rm',
        '-rf',
        TMP_DIR,
    ],
).wait()
